An implementation of a particle simulator as described in Part 1 of HW2 of [CSC267](https://www2.eecs.berkeley.edu/Courses/CSC267/), written in modern C++.

The serial implementations have no dependencies (beyond the C++ STL), whereas the OpenMP implementations depend only on OpenMP.

Build with

``mkdir build && cd build``

``meson .. --buildtype release``

``ninja all``