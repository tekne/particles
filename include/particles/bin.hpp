#ifndef PARTICLES_PARTICLE_BIN_INCLUDED
#define PARTICLES_PARTICLE_BIN_INCLUDED

#include <particles/particle.hpp>

namespace prt
{

  class Bin
  {
    std::vector<Particle*> bin;
  public:
    Bin(unsigned n): bin{} {bin.reserve(n);}

    Bin& clear() {bin.clear(); return *this;}
    Bin& push(Particle& p) {bin.push_back(&p); return *this;}
    const Bin& decelerate() const {
      for(auto p : bin) p->decelerate();
      return *this;
    }
    StepStatisticalPackage interact(Simulation& sim, const Bin& b) const {
      StepStatisticalPackage s{};
      for(auto p : bin) {
        for(auto n : b.bin) s += sim.apply_force(*p, *n);
      }
      return s;
    }
    size_t size() const {return bin.size();}
    size_t out_of_bounds(double bx, double by, double tx, double ty)
    {
      size_t c = 0;
      for(auto p : bin)
        if(!p->in_bounds(bx, by, tx, ty)) c++;
      return c;
    }
  };

}

#endif
