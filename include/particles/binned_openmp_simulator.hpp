#ifndef PARTICLES_BINNED_OPENMP_SIMULATOR_INCLUDED
#define PARTICLES_BINNED_OPENMP_SIMULATOR_INCLUDED

#include <particles/bin.hpp>
#include <particles/simulation.hpp>
#include <cmath>
#include <cassert>

#include <omp.h>

namespace prt
{

  template<unsigned load_factor>
  class BinnedOpenMPSimulator
  {
    std::vector<Bin> bins;
    unsigned side;
    double bin_size;
    unsigned num_threads;

    bool in_bounds(unsigned i, unsigned j) const {
      return i < side && j < side;
    }

    Bin* get(unsigned i, unsigned j) {
      if(!in_bounds(i, j)) return nullptr;
      return &bins[i*side + j];
    }

    double get_bx(unsigned i) const {return i * bin_size;}
    double get_by(unsigned j) const {return j * bin_size;}

    bool pos_in_bounds(const Particle& p, unsigned i, unsigned j)
    {
      return p.in_bounds(
        get_bx(i), get_by(j), get_bx(i) + bin_size, get_by(j) + bin_size);
    }

    BinnedOpenMPSimulator& clear_bins() {
      size_t no_bins = bins.size();
      #pragma omp parallel for
      for(size_t i = 0; i < no_bins; i++) bins[i].clear();
      return *this;
    }

    BinnedOpenMPSimulator& bin(Particle& p) {
      int i = p.x() / bin_size;
      int j = p.y() / bin_size;

      if(i < 0) i = 0;
      if(i >= (int)side) i = side - 1;
      if(j < 0) j = 0;
      if(j >= (int)side) j = side - 1;

      auto b = get((unsigned)i, (unsigned)j);
      assert(b);
      b->push(p);

      return *this;
    }

  public:
    BinnedOpenMPSimulator& reset(unsigned n)
    {
      bins.clear();

      unsigned bin_fraction = n / load_factor;
      if(!bin_fraction) bin_fraction = 1;

      side = (unsigned)ceil(sqrt(bin_fraction));
      bin_fraction = side * side;

      bins.reserve(bin_fraction);

      // Reserve space for load_factor particles plus a little slack
      // (to avoid memory allocations) in each bin
      for(unsigned i = 0; i < bin_fraction; i++) bins.emplace_back(
        load_factor + load_factor/3
      );

      return *this;
    }

    StepStatisticalPackage step(Simulation& sim)
    {
      StepStatisticalPackage s{};

      bin_size = sim.get_size() / side;
      int bin_radius = (int)ceil(sim.cutoff / bin_size);

      clear_bins();

      for(auto& particle : sim.particles()) bin(particle);

      #pragma omp parallel
      {
        num_threads = omp_get_num_threads();
        #pragma omp for
        for(unsigned i = 0; i < side; i++)
        {
          StepStatisticalPackage temp_s{};
          for(unsigned j = 0; j < side; j++)
          {
            auto p = get(i, j);
            assert(p);
            auto& b = *p;

            b.decelerate();
            for(int rx = -bin_radius; rx <= bin_radius; rx++)
            {
              for(int ry = -bin_radius; ry <= bin_radius; ry++)
              {
                auto nb = get(i + rx, j + ry);
                if(nb) temp_s += b.interact(sim, *nb);
              }
            }
          }
          #pragma omp critical
          {
            s += temp_s;
          }
        }
      }

      auto& parts = sim.particles();
      auto sz = parts.size();

      for(size_t i = 0; i < sz; i++)
        parts[i].move(Simulation::timestep(), sim.get_size());
      return s;
    }

    BinnedOpenMPSimulator(unsigned n)
    : bins{}, side(0), bin_size(0), num_threads(0)
    {
      if(!n) return;
      reset(n);
      printf(
        "Binned OpenMP simulator: side length %u (%zu bins, load factor %g)\n",
        side, bins.size(), (double)n/bins.size());
    }

    BinnedOpenMPSimulator& done()
    {
      size_t pbin = 0;
      for(const auto& b: bins) pbin += b.size();

      size_t mx = 0;
      size_t mn = pbin;
      for(const auto& b: bins) {
        if(b.size() < mn) mn = b.size();
        else if(b.size() > mx) mx = b.size();
      }

      double avgpbin = (double)pbin / bins.size();

      double var = 0;
      for(const auto& b: bins)
        var += (b.size() - avgpbin) * (b.size() - avgpbin);
      var /= bins.size();

      printf("\nParticles in bins (should be all): %zu\n", pbin);
      printf("Average particle/bin: %g\n", avgpbin);
      printf("Max: %zu, Min: %zu, Standard deviation: %g\n", mx, mn, sqrt(var));
      printf(
        "# of threads: %u\n",
        num_threads
      );

      return *this;
    }

    BinnedOpenMPSimulator& print_summary(FILE* f, unsigned n, double t)
    {
      fprintf(f, "%d %d %g\n", n, num_threads, t);
      return *this;
    }
  };

}

#endif
