#ifndef PARTICLES_NAIVE_OPENMP_SIMULATOR_INCLUDED
#define PARTICLES_NAIVE_OPENMP_SIMULATOR_INCLUDED

#include <particles/simulation.hpp>
#include <omp.h>

namespace prt
{

  class NaiveOpenMPSimulator
  {
    unsigned num_threads;
  public:
    NaiveOpenMPSimulator& reset(unsigned) {return *this;}
    NaiveOpenMPSimulator(unsigned n): num_threads(0)
    {
      printf("Naive OpenMP simulator for %u particles\n", n);
    }

    StepStatisticalPackage step(Simulation& sim)
    {
      StepStatisticalPackage s{};
      auto& parts = sim.particles();
      auto sz = parts.size();

      #pragma omp parallel
      {
        num_threads = omp_get_num_threads();
        #pragma omp for
        for(size_t i = 0; i < sz; i++)
        {
          StepStatisticalPackage temp_s{};
          parts[i].decelerate();
          for(auto n : sim.particles()) temp_s += sim.apply_force(parts[i], n);

          #pragma omp critical
          {
            s += temp_s;
          }
        }
      }
      #pragma omp parallel
      {
        #pragma omp for
        for(size_t i = 0; i < sz; i++)
          parts[i].move(Simulation::timestep(), sim.get_size());
      }
      return s;
    }

    NaiveOpenMPSimulator& done() {
      printf(
        "# of threads: %u\n",
        num_threads);
      return *this;
    }

    NaiveOpenMPSimulator& print_summary(FILE* f, unsigned n, double t)
    {
      fprintf(f, "%d %d %g\n", n, num_threads, t);
      return *this;
    }
  };

}


#endif
