#ifndef PARTICLES_NAIVE_SERIAL_SIMULATOR_INCLUDED
#define PARTICLES_NAIVE_SERIAL_SIMULATOR_INCLUDED

#include <particles/simulation.hpp>

namespace prt
{

  class NaiveSerialSimulator
  {
  public:
    NaiveSerialSimulator& reset(unsigned) {return *this;}
    NaiveSerialSimulator(unsigned n) {
      printf("Naive serial simulator for %u particles\n", n);
    }

    StepStatisticalPackage step(Simulation& sim)
    {
      StepStatisticalPackage s{};
      for(auto& p : sim.particles())
      {
        p.decelerate();
        for(auto n : sim.particles()) s += sim.apply_force(p, n);
      }
      for(auto& p : sim.particles())
        p.move(Simulation::timestep(), sim.get_size());
      return s;
    }

    NaiveSerialSimulator& done() {return *this;}

    NaiveSerialSimulator& print_summary(FILE* f, unsigned n, double t)
    {
      fprintf(f, "%d %g\n", n, t);
      return *this;
    }
  };

}

#endif
