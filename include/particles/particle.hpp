#ifndef PARTICLES_PARTICLE_INCLUDED
#define PARTICLES_PARTICLE_INCLUDED

#include <vector>
#include <cmath>
#include <cassert>
#include <cstdio>

namespace prt
{

  template<class Unit>
  struct Point
  {
    double x; double y;
    constexpr Point<Unit> operator+(Point<Unit> p) const {
      return {x + p.x, y + p.y};
    }
    constexpr Point<Unit>& operator+=(Point<Unit> p) {
      x += p.x;
      y += p.y;
      return *this;
    }
    constexpr Point<Unit> operator-(Point<Unit> p) const {
      return {x - p.x, y - p.y};
    }
    constexpr Point<Unit>& operator-=(Point<Unit> p) {
      x -= p.x;
      y -= p.y;
      return *this;
    }
    constexpr Point<Unit> operator-() const {
      return {-x, -y};
    }
    constexpr Point<Unit> operator*(double sc) const {return {sc*x, sc*y};}
    constexpr Point<Unit> operator/(double sc) const {return {x/sc, y/sc};}
    constexpr Point<Unit>& operator*=(double sc) {
      x *= sc;
      y *= sc;
      return *this;
    }
    constexpr Point<Unit>& operator/=(double sc) {
      x /= sc;
      y /= sc;
      return *this;
    }
    constexpr double operator*(Point<Unit> p) const {
      return x*p.x + y*p.y;
    }

    template<class OtherUnit>
    constexpr Point<OtherUnit> as() const {return {x, y};}
  };

  template<class T, class Unit>
  constexpr Point<Unit> operator*(T sc, Point<Unit> p) {return p * sc;}

  struct Position {};
  struct Velocity {};
  struct Acceleration {};

  template<size_t num, size_t denom>
  struct TimeInterval {
    constexpr double get() const {return (double)num/denom;}
  };

  template<size_t num, size_t denom>
  constexpr Point<Velocity> operator*(
    Point<Acceleration> a, TimeInterval<num, denom> t) {
    return {t.get()*a.x, t.get()*a.y};
  }
  template<size_t num, size_t denom>
  constexpr Point<Position> operator*(
    Point<Velocity> v, TimeInterval<num, denom> t) {
    return {t.get()*v.x, t.get()*v.y};
  }

  class Particle
  {
    Point<Position> position;
    Point<Velocity> velocity;
    Point<Acceleration> acceleration;
  public:
    constexpr Particle(
      Point<Position> pos,
      Point<Velocity> vel = {0, 0},
      Point<Acceleration> acc = {0, 0}
    ): position(pos), velocity(vel), acceleration(acc) {}

    constexpr Point<Position> get_pos() const {return position;}
    constexpr Point<Velocity> get_vel() const {return velocity;}
    constexpr Point<Acceleration> get_acc() const {return acceleration;}
    constexpr double x() const {return position.x;}
    constexpr double y() const {return position.y;}

    constexpr bool in_bounds(double bx, double by, double tx, double ty) const {
      return x() >= bx && x() <= tx && y() >= by && y() <= ty;
    }

    constexpr Particle& accelerate(Point<Acceleration> a) {
      acceleration += a; return *this;}
    constexpr Particle& decelerate() {
      acceleration = {0, 0}; return *this;
    }

    template<size_t num, size_t denom>
    constexpr Particle& move(TimeInterval<num, denom> t, double sz) {
      // Movement function adapted from CSC267 HW2 starter code found here:
      // https://sites.google.com/lbl.gov/cs267-spr2018/hw-2
      velocity += acceleration * t;
      position += velocity * t;

      while(position.x < 0 || position.x > sz)
      {
        position.x = position.x < 0 ? -position.x : 2*sz - position.x;
        velocity.x = -velocity.x;
      }
      while(position.y < 0 || position.y > sz)
      {
        position.y = position.y < 0 ? -position.y : 2*sz - position.y;
        velocity.y = -velocity.y;
      }

      assert(position.x >= 0 && position.x <= sz);
      assert(position.y >= 0 && position.y <= sz);

      return *this;
    }
  };

}

#endif
