#ifndef PARTICLE_SIMULATION_INCLUDED
#define PARTICLE_SIMULATION_INCLUDED

#include <particles/particle.hpp>

namespace prt
{

  struct InteractionStatisticalPackage
  {
    bool happened;
    double dmin;
    double davg;
  };

  class StepStatisticalPackage
  {
    double dmin;
    double davg;
    unsigned intr;
    unsigned aintr;
  public:
    constexpr StepStatisticalPackage()
    : dmin(1), davg(0), intr(0), aintr(0) {}

    double get_minimum_distance() const {return dmin;}
    double get_average_distance() const {return davg/intr;}
    unsigned get_interactions() const {return intr;}
    unsigned get_attempted_interactions() const {return aintr;}

    constexpr StepStatisticalPackage&
    operator+=(StepStatisticalPackage s) {
      if(s.dmin < dmin) dmin = s.dmin;
      davg += s.davg;
      intr += s.intr;
      aintr += s.aintr;
      return *this;
    }
    constexpr StepStatisticalPackage&
    operator+=(InteractionStatisticalPackage s) {
      aintr++;
      if(s.happened)
      {
        intr++;
        if(s.dmin < dmin) dmin = s.dmin;
        davg += s.davg;
      }
      return *this;
    }

    constexpr StepStatisticalPackage operator+(StepStatisticalPackage s) const {
      return s += *this;
    }
    constexpr StepStatisticalPackage operator+(InteractionStatisticalPackage s)
    const {
      StepStatisticalPackage sp = *this;
      return sp += s;
    }
  };

  class Simulation
  {
    static constexpr const double density = 0.0005;
    static constexpr const double mass = 0.01;

    std::vector<Particle> parts;
    double size;
    double absavg;
    double absmin;
    unsigned nabsavg;
    size_t intr;
    size_t aintr;

    unsigned step;
    bool first;

    Simulation& init_parts(unsigned n);

  public:
    static constexpr const double cutoff = 0.01;
    static constexpr const double min_r = cutoff/100;

    typedef TimeInterval<1, 2000> timestep;

    static double get_size(unsigned n) {return sqrt(density * n);}
    double get_size() const {return size;}
    unsigned particle_count() const {return (unsigned)parts.size();}
    unsigned get_step() {return step;}
    size_t get_interactions() {return intr;}
    size_t get_attempted_interactions() {return aintr;}

    size_t in_bounds() {
      size_t c = 0;
      for(auto& p : parts) {
        if(p.x() > 0 && p.y() > 0 && p.x() <= size && p.y() <= size) c++;
      }
      return c;
    }

    double get_minimum_distance() const {return absmin;}
    double get_average_distance() const {return absavg / nabsavg;}

    Simulation& save(FILE* f);

    Simulation& set_size(unsigned n) {
      size = get_size(n); first = true; return *this;
    }
    Simulation& set_size() {return set_size((unsigned)parts.size());}

    std::vector<Particle>& particles() {return parts;}
    const std::vector<Particle>& particles() const {return parts;}

    Simulation& reset(unsigned n) {
      set_size(n);
      return init_parts(n);
    }
    Simulation& reset() {return reset((unsigned)parts.size());}

    Simulation& compute_statistics(
      StepStatisticalPackage sp,
      FILE* fsave = nullptr, unsigned frequency = 10) {
      // Statistics function adapted from CSC267 HW2 starter code found here:
      // https://sites.google.com/lbl.gov/cs267-spr2018/hw-2
      intr += sp.get_interactions();
      aintr += sp.get_attempted_interactions();
      if(sp.get_interactions()) {
        absavg += sp.get_average_distance();
        nabsavg++;
      }
      if(sp.get_minimum_distance() < absmin) absmin = sp.get_minimum_distance();
      if(fsave && !(step % frequency)) save(fsave);
      return *this;
    }

    Simulation& step_cleanup() {
      step++;
      return *this;
    }

    InteractionStatisticalPackage apply_force(Particle& p, Particle n) {
      // Interaction function adapted from CSC267 HW2 starter code found here:
      // https://sites.google.com/lbl.gov/cs267-spr2018/hw-2
      Point<Position> displacement = n.get_pos() - p.get_pos();
      double r2 = displacement * displacement;

      double davg = 0;
      bool happened = false;

      if(r2 > cutoff * cutoff) return {false, 0, 0};

      if(r2 != 0)
      {
        davg = sqrt(r2)/cutoff;
        happened = true;
      }

      r2 = fmax(r2, min_r*min_r);
      double r = sqrt(r2);

      double coef = (1 - cutoff/r) / r2 / mass;
      p.accelerate(coef * displacement.as<Acceleration>());
      return {happened, davg, davg};
    }

    Simulation(): parts{}, size(0), absavg(0), absmin(1), nabsavg(0),
    intr(0), aintr(0),
    step(0), first(true) {}
    Simulation(unsigned n): Simulation() {reset(n);}
  };

}

#endif
