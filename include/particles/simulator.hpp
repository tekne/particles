#ifndef PARTICLES_SIMULATOR_INCLUDED
#define PARTICLES_SIMULATOR_INCLUDED

#include <particles/simulation.hpp>
#include <chrono>
#include <cstdio>

namespace prt
{

  template<class ST>
  double simulate(
    ST& simulator, Simulation& simulation, bool statistics,
    unsigned iterations = 1000,
    FILE* fsave = nullptr, unsigned frequency = 10
  ) {
    auto start = std::chrono::high_resolution_clock::now();
    while(simulation.get_step() < iterations)
    {
      auto s = simulator.step(simulation);
      if(statistics) simulation.compute_statistics(s, fsave, frequency);
      simulation.step_cleanup();
    }
    return std::chrono::duration<double>(
      std::chrono::high_resolution_clock::now() - start
    ).count();
  }

}

#endif
