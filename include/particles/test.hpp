#ifndef SIMPLEC_PARTICLE_TEST_INCLUDED
#define SIMPLEC_PARTICLE_TEST_INCLUDED

#include <particles/utility.hpp>
#include <particles/simulator.hpp>
#include <cstdio>

namespace prt
{

  template<class ST>
  int test(int argc, char** argv)
  {
    // Test function adapted from CSC267 HW2 starter code found here:
    // https://sites.google.com/lbl.gov/cs267-spr2018/hw-2
    if(find_option( argc, argv, "-h" ) >= 0)
    {
      printf( "Options:\n" );
      printf( "-h to see this help\n" );
      printf( "-n <int> to set the number of particles\n" );
      printf( "-o <filename> to specify the output file name\n" );
      printf( "-s <filename> to specify a summary file name\n" );
      printf( "-no turns off all correctness checks and particle output\n");
      return 0;
    }

    bool cs = compute_statistics(argc, argv);
    if(!cs) printf("WARNING: NOT computing statistics\n");


    unsigned n = read_int(argc, argv, "-n", 1000);
    unsigned t = read_int(argc, argv, "-t", 1000);
    const char* savename = read_string( argc, argv, "-o", NULL );
    const char* sumname = read_string( argc, argv, "-s", NULL );

    printf("Beginning test: N = %u, Steps = %u\n", n, t);

    FILE *fsave = savename ? fopen( savename, "w" ) : NULL;
    FILE *fsum = sumname ? fopen ( sumname, "a" ) : NULL;

    Simulation S{n};
    printf("Simulation initialized: size %g\n", S.get_size());

    ST N{n};
    double simulation_time = simulate(N, S, cs, t, fsave);

    N.done();

    printf(
      "\nSimulation Time = %g seconds\n",
      simulation_time
    );

    size_t pib = S.in_bounds();

    printf(
      "Sanity check: particles in bounds: %zu (%g%%)\n",
      pib,
      100*(double)pib/n
    );

    size_t aintr = S.get_attempted_interactions();
    size_t pintr = (size_t)n*n*t;

    printf(
      "Potential interactions: %zu, Attempted interactions: %zu (%g%%)\n",
      pintr, aintr, 100*(double)aintr/pintr
    );
    if(cs) {
      double absmin = S.get_minimum_distance();
      double absavg = S.get_average_distance();
      size_t intr = S.get_interactions();

      printf( "Interactions: %zu (%g%%, %g%%)\n",
        intr, 100*(double)intr/aintr, 100*(double)intr/pintr
      );
      printf("Minimum distance: %g, Average distance: %g\n", absmin, absavg);
      if (absmin < 0.4) printf(
        "\nThe minimum distance is below 0.4 meaning that some particle is not interacting"
      );
      if (absavg < 0.8) printf (
        "\nThe average distance is below 0.8 meaning that most particles are not interacting"
      );
    }
    putchar('\n');

    if(fsum) N.print_summary(fsum, n, simulation_time);

    if(fsum) fclose(fsum);
    if(fsave) fclose(fsave);

    return 0;
  }


}

#endif
