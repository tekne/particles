#ifndef PARTICLES_UTILITY_INCLUDED
#define PARTICLES_UTILITY_INCLUDED

namespace prt
{
  int find_option(int argc, char** argv, const char* option);
  int read_int(int argc, char** argv, const char* option, int default_value);
  char* read_string(
    int argc, char** argv, const char* option, char* default_value);
  bool compute_statistics(int argc, char** argv);
}

#endif
