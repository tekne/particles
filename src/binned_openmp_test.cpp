#include <particles/test.hpp>
#include <particles/binned_openmp_simulator.hpp>
#include <cstdio>

int main(int argc, char** argv)
{
  return prt::test<prt::BinnedOpenMPSimulator<4>>(argc, argv);
}
