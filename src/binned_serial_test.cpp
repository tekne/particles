#include <particles/test.hpp>
#include <particles/binned_serial_simulator.hpp>
#include <cstdio>

int main(int argc, char** argv)
{
  return prt::test<prt::BinnedSerialSimulator<4>>(argc, argv);
}
