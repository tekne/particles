#include <particles/test.hpp>
#include <particles/naive_openmp_simulator.hpp>
#include <cstdio>

int main(int argc, char** argv)
{
  return prt::test<prt::NaiveOpenMPSimulator>(argc, argv);
}
