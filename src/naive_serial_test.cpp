#include <particles/test.hpp>
#include <particles/naive_serial_simulator.hpp>
#include <cstdio>

int main(int argc, char** argv)
{
  return prt::test<prt::NaiveSerialSimulator>(argc, argv);
}
