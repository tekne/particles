#include <particles/simulation.hpp>
#include <cmath>
#include <cstdlib>
#include <ctime>

namespace prt
{

  Simulation& Simulation::save(FILE* f) {
    // Save function adapted from CSC267 HW2 starter code found here:
    // https://sites.google.com/lbl.gov/cs267-spr2018/hw-2
    if(first)
    {
      first = false;
      fprintf(f, "%d %g\n", (unsigned)parts.size(), size);
    }
    for(const auto& p : parts) fprintf(f, "%g %g\n", p.x(), p.y());
    return *this;
  }

  Simulation& Simulation::init_parts(unsigned n)
  {
    // Initialization function adapted from CSC267 HW2 starter code found here:
    // https://sites.google.com/lbl.gov/cs267-spr2018/hw-2
    parts.clear();
    parts.reserve(n);

    srand48(time(nullptr));

    unsigned sx = (unsigned)ceil(sqrt(n));
    unsigned sy = (n + sx-1)/sx;

    std::vector<unsigned> shuffle;
    shuffle.resize(n);
    for(size_t i = 1; i < shuffle.size(); i++) shuffle[i] = i;

    for(size_t i = 0; i < n; i++)
    {
      int j = lrand48()%(n-i);
      int k = shuffle[j];
      shuffle[j] = shuffle[n-i-1];

      parts.push_back({
        {size*(1.+(k%sx))/(1+sx), size*(1.+(k/sx))/(1+sy)},
        {drand48()*2-1, drand48()*2-1}
      });
    }
    return *this;
  }

}
